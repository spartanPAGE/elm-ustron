module Router exposing (Sitemap)

import Route exposing (static, (:=))

type Sitemap
  = Home ()
  | NotFound ()

homeRoute =
  Home := static ""

notFound =
  NotFound := static "notFound"
