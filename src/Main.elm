module Main exposing (..)

import Html.App as Html
import Update exposing (Flags, init, update, Msg(..))
import View exposing (view)


main =
  Html.programWithFlags
  { init = init
  , update = update
  , view = view
  , subscriptions =
    \_ ->
      Sub.batch [path PathChanged]
  }
