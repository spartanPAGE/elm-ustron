module Update exposing (Flags, Msg, init, update)


import Router exposing(Sitemap(..))


type alias Flags =
  { path: String
  }


type alias Model =
  { route: Sitemap
  }

type Msg
  = PathChanged String
  | RouteTo Sitemap

initialModel =
  {
  }

messages =
  0

init =
  (initialModel, Cmd.none)

update msg model =
  init
